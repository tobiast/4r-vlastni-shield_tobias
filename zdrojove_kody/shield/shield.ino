#include <LiquidCrystal_I2C.h>
#include <Adafruit_NeoPixel.h>
#include <OneWire.h>
#include <DallasTemperature.h>

#define LED_GREEN D5   
#define LED_RED D6     
#define TEMP_SENSOR_PIN D7  
#define LED_MATRIX_PIN D8   
#define SDA_PIN D2    
#define SCL_PIN D1  
#define PHOTORESISTOR_PIN A0  


Adafruit_NeoPixel strip = Adafruit_NeoPixel(8, LED_MATRIX_PIN, NEO_GRB + NEO_KHZ800);

LiquidCrystal_I2C lcd(0x27, 16, 2);

OneWire oneWire(TEMP_SENSOR_PIN);
DallasTemperature sensors(&oneWire);

const float TEMP_THRESHOLD = 22.0; 
const int LIGHT_THRESHOLD = 300;  

void setup() {
  
  Serial.begin(9600);

 
  pinMode(LED_GREEN, OUTPUT);
  pinMode(LED_RED, OUTPUT);

  strip.begin();
  strip.show();

  
  Wire.begin(SDA_PIN, SCL_PIN);

  lcd.init();        
  lcd.backlight();   

  sensors.begin();
}

void loop() {
  // Update LED matrix with new effects
  updateLEDMatrix();
  teplota();
  svetlo();
}

void updateLEDs(float temperature) {
  if (temperature > TEMP_THRESHOLD) {
    digitalWrite(LED_GREEN, HIGH);
    digitalWrite(LED_RED, LOW);
  } else {
    digitalWrite(LED_GREEN, LOW);
    digitalWrite(LED_RED, HIGH);
  }
}

void teplota(){

  Serial.print("Získávám teplotu...");
  sensors.requestTemperatures();  
  Serial.println("!!HOTOVO!!");

  float temperatureC = sensors.getTempCByIndex(0);

  // Check if reading was successful
  if (temperatureC != DEVICE_DISCONNECTED_C) {
    
    lcd.setCursor(0, 0);
    lcd.print("Teplota: ");
    lcd.print(temperatureC);
    lcd.print(" C");

    Serial.print("Teplota je: ");
    Serial.println(temperatureC);

    updateLEDs(temperatureC);
  } else {
    Serial.println("!!!Error: Nejde to!!!");
    lcd.setCursor(0, 0);
    lcd.print("Nefunguje to!       "); 
  }
}

void svetlo() {
  int lightLevel = analogRead(PHOTORESISTOR_PIN);  
  Serial.println(lightLevel);

  lcd.setCursor(0, 1);
  lcd.print("Svetlo:       ");  
  lcd.setCursor(7, 1);

  if (lightLevel > LIGHT_THRESHOLD) {
    lcd.print("Den");  
  } else {
    lcd.print("Noc");
  }  

  delay(500);
}

void updateLEDMatrix() {
  static int hue = 0;  // Udržíme aktuální odstín, aby se barvy plynule měnily
  for (int i = 0; i < strip.numPixels(); i++) {
    int pixelHue = hue + (i * 256 / strip.numPixels());  // Rozdílné odstíny pro každý pixel
    strip.setPixelColor(i, Wheel(pixelHue & 255));       // Získání barvy podle odstínu
  }
  strip.show();
  hue += 10;  // Rychlejší posun odstínu pro svižnější přechod
  delay(20);  // Zmenšení zpoždění pro rychlejší animaci
}

// Funkce pro vytvoření barevného přechodu na základě odstínu (hue)
uint32_t Wheel(byte WheelPos) {
  WheelPos = 255 - WheelPos;
  if (WheelPos < 85) {
    return strip.Color(255 - WheelPos * 3, 0, WheelPos * 3);
  } else if (WheelPos < 170) {
    WheelPos -= 85;
    return strip.Color(0, WheelPos * 3, 255 - WheelPos * 3);
  } else {
    WheelPos -= 170;
    return strip.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
  }
}