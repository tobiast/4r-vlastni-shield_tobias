# Otázky na prezentaci
**Zodpovězte prosím pro třídu pravdivě následující otázky**
| otázka | vyjádření |
| :--- | :--- |
| jak dlouho mi tvorba zabrala - **15H** | |
| jak se mi to podařilo rozplánovat | Nijak jsem to neplánoval spíš prokrastinoval.|
| design zapojení | ** |
| proč jsem zvolil tento design | Líbí se mi to. |
| zapojení | ** |
| z jakých součástí se zapojení skládá | fotorezistor, LED zelená a červená, LED pásek, LCD displej, teploměr, shield a wemos |
| realizace | ** |
| nápad, v jakém produktu vše propojit dohromady| ????|
| co se mi povedlo | Všechno okolo projektu.|
| co se mi nepovedlo/příště bych udělal/a jinak |Nic vše mám dokonalé. |
| zhodnocení celé tvorby | Tvorbu bych zhodnotil velice pozitivně, jelikož vše funguje jak má. |